var direct_domains = {
    "qunar.com": 1,
    "tujia.com": 1,
    "ctrip.com": 1
};
 
var proxy = "DIRECT; SOCKS5 proxy.corp.qunar.com:10080;";
var direct = "DIRECT;";
 
function FindProxyForURL(url, host) {
    host = host.toLowerCase();
    var lastPos;
    do {
        if (direct_domains.hasOwnProperty(host)) {
            return direct;
        }
        lastPos = host.indexOf('.') + 1;
        host = host.slice(lastPos);
    } while (lastPos >= 1);
    return proxy;
}