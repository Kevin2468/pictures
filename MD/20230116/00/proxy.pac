var proxy = "SOCKS5 proxy.corp.qunar.com:10080; DIRECT;";
var direct = "DIRECT;";
 
function FindProxyForURL(url, host) {
    host = host.toLowerCase();
    var lastPos;
    do {
        if (/^127\.0\.0\.1$/.test(host) || /^::1$/.test(host) || /^localhost$/.test(host)) return direct;
        lastPos = host.indexOf('.') + 1;
        host = host.slice(lastPos);
    } while (lastPos >= 1);

    return proxy;
}