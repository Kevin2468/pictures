var domains = {
    "google.com": 1,
    "facebook.com": 1,
    "youtube.com": 1,
    "twitter.com": 1,
    "xvideos.com":1
};
 
var proxy = "SOCKS5 proxy.corp.qunar.com:10080; SOCKS proxy.corp.qunar.com:10080;";
 
var direct = "DIRECT;";
 
function FindProxyForURL(url, host) {
    url  = url.toLowerCase();
    host = host.toLowerCase();

    var lastPos;
    do {
   
        if (/^127\.0\.0\.1$/.test(host) || /^::1$/.test(host) || /^localhost$/.test(host)) return direct;

        if (domains.hasOwnProperty(host)) {
            return proxy;
        }
        lastPos = host.indexOf('.') + 1;
        host = host.slice(lastPos);
    } while (lastPos >= 1);
    return proxy;
}